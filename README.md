# Browser Automation

## Puppeteer and adaptations

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-puppeteer)
- [Bloggo post](http://hubelbauer.net/post/bloggo-puppeteer)

- [Firefox Puppeteer adaptation](https://github.com/autonome/puppeteer-fx)
- [Not an Edge adaptation](https://www.npmjs.com/package/puppeteer-edge)
- [IE Puppeteer API](https://github.com/TechQuery/Puppeteer-IE)

## Selenium :-1:

- [Edge stuff](https://docs.microsoft.com/en-us/microsoft-edge/dev-guide/tools/webdriver)
